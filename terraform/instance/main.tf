provider "aws" {
  region  = "${var.region}"
}

# I typically create an IAM role and instance profile for
# every instance (or logical group of instances) so that IAM
# permissions can be easily granted/managed in the future
resource "aws_iam_role" "role" {
  name                = "crossroads-ops-instance"
  assume_role_policy  = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "profile" {
  name  = "crossroads-ops-instance"
  role  = "${aws_iam_role.role.name}"
}

# Security best practice to encrypt EBS volumes,
# so I'll create a KMS key for that
resource "aws_kms_key" "ebs_key" {
  depends_on          = ["aws_iam_role.role"]
  description         = "EBS FDE key for crossroads ops project"
  enable_key_rotation = true
  policy              = "${file("ebs_key_policy.json")}"
}

resource "aws_kms_alias" "alias" {
  name          = "alias/crossroads-ops-ebs-key"
  target_key_id = "${aws_kms_key.ebs_key.key_id}"
}

resource "aws_instance" "instance" {
  ami                     = "${var.ami_id}"
  availability_zone       = "${join("", list(var.region, var.az))}"
  instance_type           = "t2.micro"
  key_name                = "crossroadsops"
  vpc_security_group_ids  = ["${var.security_group_ids}"]
  subnet_id               = "${var.subnet_id}"
  iam_instance_profile    = "${aws_iam_instance_profile.profile.name}"

  tags {
    Name  = "crossroads-ops-instance"
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = 8
  }
}

# I could do a "loop" here to create the four EBS volumes,
# but I've leanred that you should be careful doing this type
# of stuff with Terraform because resources created in groups
# are hard to manage individually later on. Instead, I'll use
# a module to keep it a little cleaner here.
module "ebs_vol_f" {
  source      = "../modules/ebs_vol"
  az          = "${join("", list(var.region, var.az))}"
  size        = 10
  kms_key_id  = "${aws_kms_key.ebs_key.arn}"
  instance_id = "${aws_instance.instance.id}"
  mount_point = "/dev/xvdf"
}

module "ebs_vol_g" {
  source      = "../modules/ebs_vol"
  az          = "${join("", list(var.region, var.az))}"
  size        = 10
  kms_key_id  = "${aws_kms_key.ebs_key.arn}"
  instance_id = "${aws_instance.instance.id}"
  mount_point = "/dev/xvdg"
}

module "ebs_vol_h" {
  source      = "../modules/ebs_vol"
  az          = "${join("", list(var.region, var.az))}"
  size        = 10
  kms_key_id  = "${aws_kms_key.ebs_key.arn}"
  instance_id = "${aws_instance.instance.id}"
  mount_point = "/dev/xvdh"
}

module "ebs_vol_i" {
  source      = "../modules/ebs_vol"
  az          = "${join("", list(var.region, var.az))}"
  size        = 10
  kms_key_id  = "${aws_kms_key.ebs_key.arn}"
  instance_id = "${aws_instance.instance.id}"
  mount_point = "/dev/xvdi"
}

# Output the private IP of the instance so I know how to get into it
output "private_ip" {
  value = "${aws_instance.instance.private_ip}"
}
