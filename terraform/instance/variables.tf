variable "region" {
  default = "us-east-1"
}

variable "az" {
  default = "a"
}

# Use the latest Amazon Linux AMI
variable "ami_id" {
  default = "ami-a4c7edb2"
}

# A private subnet from my personal AWS account
variable "subnet_id" {
  default = "subnet-ff251f89"
}

variable "security_group_ids" {
  default = [
    "sg-5dbfde26"
  ]
}
