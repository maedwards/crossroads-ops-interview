resource "aws_ebs_volume" "volume" {
  availability_zone = "${var.az}"
  encrypted         = true
  size              = "${var.size}"
  type              = "gp2"
  kms_key_id        = "${var.kms_key_id}"
}

resource "aws_volume_attachment" "attachment" {
  device_name = "${var.mount_point}"
  instance_id = "${var.instance_id}"
  volume_id   = "${aws_ebs_volume.volume.id}"
}
