variable "az" {} # Availability zone
variable "size" {} # Volume size in GB
variable "kms_key_id" {}
variable "instance_id" {}
variable "mount_point" {}
